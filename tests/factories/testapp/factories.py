# Django
from django.utils.text import slugify

# Third party
import factory
from factory.django import DjangoModelFactory

# Project
from tests.testapp.models import (
    DraftableObject,
    DraftableObjectForeignKey,
    DraftableObjectManyToMany,
    DraftableObjectManyToMany2,
    DraftableObjectManyToMany2Through,
    DraftableObjectOneToOne,
    DraftableObjectOneToOneRelated,
)


class DraftableObjectForeignKeyFactory(DjangoModelFactory):
    class Meta:
        model = DraftableObjectForeignKey

    label = factory.Sequence(
        lambda n: f"draftable object foreign key label {int(n) + 1}"
    )


class DraftableObjectOneToOneFactory(DjangoModelFactory):
    class Meta:
        model = DraftableObjectOneToOne

    label = factory.Sequence(
        lambda n: f"draftable object one to one label {int(n) + 1}"
    )


class DraftableObjectOneToOneRelatedFactory(DjangoModelFactory):
    draftable_object = factory.SubFactory(
        "factories.testapp.factories.DraftableObjectFactory"
    )

    class Meta:
        model = DraftableObjectOneToOneRelated

    label = factory.Sequence(
        lambda n: f"draftable object one to one related label {int(n) + 1}"
    )


class DraftableObjectManyToManyFactory(DjangoModelFactory):
    class Meta:
        model = DraftableObjectManyToMany

    label = factory.Sequence(lambda n: f"draftable object m2m label {int(n) + 1}")


class DraftableObjectManyToMany2Factory(DjangoModelFactory):
    class Meta:
        model = DraftableObjectManyToMany2

    label = factory.Sequence(lambda n: f"draftable object m2m 2 label {int(n) + 1}")


class DraftableObjectManyToManyThrough2Factory(DjangoModelFactory):
    class Meta:
        model = DraftableObjectManyToMany2Through

    draftable_object = factory.SubFactory(
        "factories.testapp.factories.DraftableObjectFactory"
    )
    draftable_object_many_to_many_2 = factory.SubFactory(
        "factories.testapp.factories.DraftableObjectManyToMany2Factory"
    )
    label = factory.Sequence(
        lambda n: f"draftable object m2m through 2 label {int(n) + 1}"
    )
    foreign_key_object = factory.SubFactory(DraftableObjectForeignKeyFactory)
    other_draftable_object = factory.SubFactory(
        "factories.testapp.factories.DraftableObjectFactory"
    )


class DraftableObjectFactory(DjangoModelFactory):
    class Meta:
        model = DraftableObject
        skip_postgeneration_save = True

    label = factory.Sequence(lambda n: f"draftable object label {int(n) + 1}")

    slug = factory.Sequence(lambda n: slugify(f"draftable object label {int(n) + 1}"))

    content = "draftable object content"
    foreign_key_object = factory.SubFactory(DraftableObjectForeignKeyFactory)
    one_to_one_object = factory.SubFactory(DraftableObjectOneToOneFactory)

    @factory.post_generation
    def many_to_many_objects(self, create, extracted, **kwargs):
        if not create or not extracted:
            # Simple build, or nothing to add, do nothing.
            return

        many_to_many_objects = DraftableObjectManyToManyFactory.create_batch(
            len(extracted)
        )
        self.many_to_many_objects.add(*many_to_many_objects)

    @factory.post_generation
    def many_to_many_2_objects_through_objects(self, create, extracted, **kwargs):
        if not create or not extracted:
            # Simple build, or nothing to add, do nothing.
            return

        other_draftable_object_slug = "other-draftable-object"
        other_draftable_object = DraftableObject.objects.filter(
            slug=other_draftable_object_slug
        ).first() or DraftableObjectFactory(slug=other_draftable_object_slug)

        DraftableObjectManyToManyThrough2Factory.create_batch(
            len(extracted),
            draftable_object=self,
            other_draftable_object=other_draftable_object,
        )

    @factory.post_generation
    def one_to_one_related_object(self, create, extracted, **kwargs):
        if not create or not extracted:
            # Simple build, or nothing to add, do nothing.
            return

        DraftableObjectOneToOneRelatedFactory(draftable_object=self)


class DraftDraftableObjectFactory(DraftableObjectFactory):
    @classmethod
    def _get_manager(cls, model_class):
        return DraftableObject.drafts


class PublicDraftableObjectFactory(DraftableObjectFactory):
    @classmethod
    def _get_manager(cls, model_class):
        return DraftableObject.publics
