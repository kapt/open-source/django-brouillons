# Project
from django_brouillons.models import VersionStatus
from tests.factories.testapp.factories import (
    DraftDraftableObjectFactory,
    PublicDraftableObjectFactory,
)


def create_draftable_object(version_status, **create_kwargs):
    if version_status == VersionStatus.DRAFT:
        draftable_object = DraftDraftableObjectFactory(
            **create_kwargs,
        )
    else:
        draftable_object = PublicDraftableObjectFactory(
            **create_kwargs,
        )

    return draftable_object
