# Django
from django.db import models

# Project
from django_brouillons.models import DraftModel


class DraftableObject(DraftModel):
    _clone_m2o_or_o2m_fields = ["draftable_object_many_to_many2_through"]
    _clone_linked_m2m_fields = ["many_to_many_objects"]
    _draft_prefixed_fields = ["slug"]

    label = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    content = models.TextField()

    foreign_key_object = models.ForeignKey(
        "DraftableObjectForeignKey", on_delete=models.CASCADE
    )
    one_to_one_object = models.OneToOneField(
        "DraftableObjectOneToOne", on_delete=models.CASCADE
    )

    many_to_many_objects = models.ManyToManyField("DraftableObjectManyToMany")
    many_to_many_2_objects_through = models.ManyToManyField(
        "DraftableObjectManyToMany2",
        through="DraftableObjectManyToMany2Through",
        through_fields=("draftable_object", "draftable_object_many_to_many_2"),
    )

    class Meta:
        app_label = "testapp"


class DraftableObjectForeignKey(models.Model):
    label = models.CharField(max_length=255)

    class Meta:
        app_label = "testapp"


class DraftableObjectOneToOne(models.Model):
    label = models.CharField(max_length=255)

    class Meta:
        app_label = "testapp"


class DraftableObjectOneToOneRelated(models.Model):
    label = models.CharField(max_length=255)

    draftable_object = models.OneToOneField(
        "DraftableObject",
        on_delete=models.CASCADE,
        related_name="draftable_object_one_to_one_related",
    )

    class Meta:
        app_label = "testapp"


class DraftableObjectManyToMany(models.Model):
    label = models.CharField(max_length=255)

    class Meta:
        app_label = "testapp"


class DraftableObjectManyToMany2(models.Model):
    label = models.CharField(max_length=255)

    class Meta:
        app_label = "testapp"


class DraftableObjectManyToMany2Through(DraftModel, models.Model):
    _clone_excluded_m2o_or_o2m_fields = [
        "draftable_object",
        "draftable_object_many_to_many_2",
        "foreign_key_object",
        "other_draftable_object",
    ]

    draftable_object = models.ForeignKey(
        "DraftableObject",
        on_delete=models.CASCADE,
        related_name="draftable_object_many_to_many2_through",
    )

    draftable_object_many_to_many_2 = models.ForeignKey(
        "DraftableObjectManyToMany2",
        on_delete=models.CASCADE,
        related_name="draftable_object_many_to_many2_through",
    )

    label = models.CharField(max_length=255)

    foreign_key_object = models.ForeignKey(
        "DraftableObjectForeignKey", on_delete=models.CASCADE
    )

    other_draftable_object = models.ForeignKey(
        "DraftableObject",
        on_delete=models.CASCADE,
        related_name="other_draftable_object_many_to_many_through",
    )

    class Meta:
        app_label = "testapp"
