# Third party
import pytest

# Project
from django_brouillons.models import VersionStatus
from tests.conftest import create_draftable_object


@pytest.mark.django_db
def test_create_public_only_version():
    public_draftable_object = create_draftable_object(
        version_status=VersionStatus.PUBLIC
    )
    assert public_draftable_object.version_status == VersionStatus.PUBLIC


@pytest.mark.django_db
def test_create_draft_only_version():
    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)
    assert draft_draftable_object.version_status == VersionStatus.DRAFT
