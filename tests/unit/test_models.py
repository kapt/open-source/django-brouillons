# Django
from django.core.exceptions import ObjectDoesNotExist
from django.utils.text import slugify
from django.utils.timezone import now

# Third party
import pytest

# Project
from django_brouillons.exceptions import DraftVersionExistsError, InvalidVersionError
from django_brouillons.models import VersionStatus
from tests.conftest import create_draftable_object
from tests.factories.testapp.factories import (
    DraftableObjectFactory,
    DraftableObjectForeignKeyFactory,
    DraftableObjectManyToManyFactory,
    DraftableObjectManyToManyThrough2Factory,
)
from tests.testapp.models import DraftableObjectOneToOneRelated


@pytest.mark.django_db
def test_is_public_is_draft_properties():
    public_draftable_object = create_draftable_object(
        version_status=VersionStatus.PUBLIC
    )

    assert public_draftable_object.is_public is True
    assert public_draftable_object.is_draft is False

    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)

    assert draft_draftable_object.is_draft is True
    assert draft_draftable_object.is_public is False


@pytest.mark.django_db
def test_has_public_version_property():
    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)
    assert draft_draftable_object.has_public_version is False

    public_draftable_object = draft_draftable_object.publish_draft()
    assert draft_draftable_object.has_public_version is True
    assert draft_draftable_object.public_version == public_draftable_object


@pytest.mark.django_db
def test_has_draft_version_property():
    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)
    public_draftable_object = draft_draftable_object.publish_draft()

    assert draft_draftable_object.has_draft_version is False
    assert public_draftable_object.has_draft_version is True


@pytest.mark.django_db
def test_has_unpublished_changes_property():
    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)

    assert draft_draftable_object.has_unpublished_changes is True
    public_draftable_object = draft_draftable_object.publish_draft()
    assert draft_draftable_object.has_unpublished_changes is False
    assert public_draftable_object.has_unpublished_changes is False

    draft_draftable_object.label = "new label"
    draft_draftable_object.save()
    assert draft_draftable_object.has_unpublished_changes is True
    assert public_draftable_object.has_unpublished_changes is True

    draft_draftable_object.publish_draft()
    public_draftable_object.refresh_from_db()
    assert draft_draftable_object.has_unpublished_changes is False
    assert public_draftable_object.has_unpublished_changes is False


@pytest.mark.django_db
def test_draft_object_slug():
    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)
    public_draftable_object = draft_draftable_object.publish_draft()

    assert draft_draftable_object.slug.startswith(draft_draftable_object._draft_prefix)
    assert public_draftable_object.slug == slugify(draft_draftable_object.label)

    public_draftable_object = draft_draftable_object.publish_draft()
    assert public_draftable_object.slug == slugify(draft_draftable_object.label)


@pytest.mark.django_db
def test_publication_dates():
    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)

    now_dt = now()

    public_draftable_object = draft_draftable_object.publish_draft()

    assert (
        draft_draftable_object.first_publication_date
        == public_draftable_object.first_publication_date
    )

    assert (
        draft_draftable_object.last_publication_date
        == public_draftable_object.last_publication_date
    )

    assert draft_draftable_object.last_publication_date > now_dt


@pytest.mark.django_db
def test_publication_does_not_change_last_edition_date():
    draft_draftable_object = create_draftable_object(version_status=VersionStatus.DRAFT)
    public_draftable_object = draft_draftable_object.publish_draft()

    assert (
        draft_draftable_object.last_publication_date
        > draft_draftable_object.last_edition_date
    )

    assert (
        public_draftable_object.last_edition_date
        == public_draftable_object.last_publication_date
    )

    public_draftable_object = create_draftable_object(
        version_status=VersionStatus.PUBLIC
    )

    draft_draftable_object = public_draftable_object.create_draft()
    draft_draftable_object.publish_draft()

    assert (
        public_draftable_object.last_publication_date
        < public_draftable_object.last_edition_date
    )

    assert (
        draft_draftable_object.last_publication_date
        > draft_draftable_object.last_edition_date
    )


def assert_draft_and_public_draftable_objects_are_equal(
    draft_draftable_object, public_draftable_object
):
    assert draft_draftable_object.label == public_draftable_object.label
    assert draft_draftable_object.content == public_draftable_object.content
    assert (
        draft_draftable_object.foreign_key_object_id
        and draft_draftable_object.foreign_key_object_id
        == public_draftable_object.foreign_key_object_id
    )

    draft_draftable_object_one_to_one = draft_draftable_object.one_to_one_object
    public_draftable_object_one_to_one = public_draftable_object.one_to_one_object

    assert (
        draft_draftable_object_one_to_one.label
        == public_draftable_object_one_to_one.label
    )

    # O2O can't have the same pk
    assert draft_draftable_object_one_to_one.pk != public_draftable_object_one_to_one.pk

    try:
        draft_draftable_object_one_to_one_related = (
            draft_draftable_object.draftable_object_one_to_one_related
        )
    except DraftableObjectOneToOneRelated.DoesNotExist:
        draft_draftable_object_one_to_one_related = None

    try:
        public_draftable_object_one_to_one_related = (
            public_draftable_object.draftable_object_one_to_one_related
        )
    except DraftableObjectOneToOneRelated.DoesNotExist:
        public_draftable_object_one_to_one_related = None

    if draft_draftable_object_one_to_one_related is None:
        assert public_draftable_object_one_to_one_related is None
    else:
        assert (
            draft_draftable_object_one_to_one_related.label
            == public_draftable_object_one_to_one_related.label
        )

        # O2O can't have the same pk
        assert (
            draft_draftable_object_one_to_one_related.pk
            != public_draftable_object_one_to_one_related.pk
        )

    assert (
        draft_draftable_object.many_to_many_objects.count()
        == public_draftable_object.many_to_many_objects.count()
    )

    assert set(
        draft_draftable_object.many_to_many_objects.values_list("pk", flat=True)
    ) == set(public_draftable_object.many_to_many_objects.values_list("pk", flat=True))

    assert (
        draft_draftable_object.many_to_many_2_objects_through.count()
        == public_draftable_object.many_to_many_2_objects_through.count()
    )

    assert set(
        draft_draftable_object.many_to_many_2_objects_through.values_list(
            "pk", flat=True
        )
    ) == set(
        public_draftable_object.many_to_many_2_objects_through.values_list(
            "pk", flat=True
        )
    )

    for (
        draft_many_to_many_2_object_through
    ) in draft_draftable_object.draftable_object_many_to_many2_through.all():
        public_many_to_many_2_object_through = public_draftable_object.draftable_object_many_to_many2_through.get(
            draftable_object_many_to_many_2=draft_many_to_many_2_object_through.draftable_object_many_to_many_2
        )

        assert (
            public_many_to_many_2_object_through.label
            == draft_many_to_many_2_object_through.label
        )

        assert (
            public_many_to_many_2_object_through.foreign_key_object
            == draft_many_to_many_2_object_through.foreign_key_object
        )

        assert (
            public_many_to_many_2_object_through.other_draftable_object
            == draft_many_to_many_2_object_through.other_draftable_object
        )


@pytest.mark.django_db
def test_can_publish_draft_when_draft_had_no_public():
    draft_draftable_object = create_draftable_object(
        version_status=VersionStatus.DRAFT,
        many_to_many_objects=["m2m1", "m2m2", "m2m3"],
        many_to_many_2_objects_through_objects=["m2mt1", "m2mt2", "m2mt3"],
        one_to_one_related_object=True,
    )

    assert draft_draftable_object.has_public_version is False
    public_draftable_object = draft_draftable_object.publish_draft()

    assert draft_draftable_object.has_public_version is True

    assert draft_draftable_object.draftable_object_one_to_one_related
    assert public_draftable_object.draftable_object_one_to_one_related

    assert draft_draftable_object.many_to_many_objects.count() == 3
    assert public_draftable_object.many_to_many_objects.count() == 3

    assert draft_draftable_object.many_to_many_2_objects_through.count() == 3
    assert public_draftable_object.many_to_many_2_objects_through.count() == 3

    assert_draft_and_public_draftable_objects_are_equal(
        draft_draftable_object, public_draftable_object
    )


@pytest.mark.django_db
def test_can_publish_draft_when_draft_had_public():
    # Test test_can_publish_draft_when_draft_had_no_public() is a prerequisite for this test
    draft_draftable_object = create_draftable_object(
        version_status=VersionStatus.DRAFT,
        many_to_many_objects=["m2m1", "m2m2", "m2m3"],
        many_to_many_2_objects_through_objects=["m2mt1", "m2mt2", "m2mt3"],
        one_to_one_related_object=True,
    )
    public_draftable_object = draft_draftable_object.publish_draft()

    # Update some values
    new_foreign_key_object = DraftableObjectForeignKeyFactory()
    new_many_to_many_objects = DraftableObjectManyToManyFactory.create_batch(2)

    draft_draftable_object.label = "new label"
    draft_draftable_object.content = "new content"
    draft_draftable_object.foreign_key_object = new_foreign_key_object
    draft_draftable_object.save()
    draft_draftable_object.many_to_many_objects.set(new_many_to_many_objects)
    draft_draftable_object.draftable_object_many_to_many2_through.all().delete()
    DraftableObjectManyToManyThrough2Factory.create_batch(
        2, draftable_object=draft_draftable_object
    )

    draft_draftable_object_one_to_one_related = (
        draft_draftable_object.draftable_object_one_to_one_related
    )
    draft_draftable_object_one_to_one_related.label = "new label"
    draft_draftable_object_one_to_one_related.save()

    draft_draftable_object_one_to_one = draft_draftable_object.one_to_one_object
    draft_draftable_object_one_to_one.label = "new label"
    draft_draftable_object_one_to_one.save()

    # Check that values are different from public version
    draft_draftable_object.refresh_from_db()
    public_draftable_object.refresh_from_db()

    assert draft_draftable_object.label != public_draftable_object.label
    assert draft_draftable_object.content != public_draftable_object.content
    assert (
        draft_draftable_object.foreign_key_object
        != public_draftable_object.foreign_key_object
    )

    draft_draftable_object_one_to_one_related = (
        draft_draftable_object.draftable_object_one_to_one_related
    )

    public_draftable_object_one_to_one_related = (
        public_draftable_object.draftable_object_one_to_one_related
    )

    assert (
        draft_draftable_object_one_to_one_related.label
        != public_draftable_object_one_to_one_related.label
    )

    assert draft_draftable_object.many_to_many_objects.count() == 2
    assert public_draftable_object.many_to_many_objects.count() == 3

    assert (
        set(draft_draftable_object.many_to_many_objects.values_list("pk", flat=True))
        & set(public_draftable_object.many_to_many_objects.values_list("pk", flat=True))
        == set()
    )

    assert draft_draftable_object.many_to_many_2_objects_through.count() == 2
    assert public_draftable_object.many_to_many_2_objects_through.count() == 3

    assert (
        set(
            draft_draftable_object.many_to_many_2_objects_through.values_list(
                "pk", flat=True
            )
        )
        & set(
            public_draftable_object.many_to_many_2_objects_through.values_list(
                "pk", flat=True
            )
        )
        == set()
    )

    # Publish draft and check that values are now the same as public version
    draft_draftable_object.publish_draft()
    public_draftable_object.refresh_from_db()

    assert draft_draftable_object.many_to_many_objects.count() == 2
    assert public_draftable_object.many_to_many_objects.count() == 2

    assert draft_draftable_object.many_to_many_2_objects_through.count() == 2
    assert public_draftable_object.many_to_many_2_objects_through.count() == 2

    assert_draft_and_public_draftable_objects_are_equal(
        draft_draftable_object, public_draftable_object
    )


@pytest.mark.django_db
def test_can_create_public_with_no_draft():
    public_draftable_object = create_draftable_object(
        version_status=VersionStatus.PUBLIC,
        many_to_many_objects=["m2m1", "m2m2", "m2m3"],
        many_to_many_2_objects_through_objects=["m2mt1", "m2mt2", "m2mt3"],
        one_to_one_related_object=True,
    )

    assert public_draftable_object.is_public
    assert not public_draftable_object.is_draft
    assert public_draftable_object.draft_version is None

    with pytest.raises(ObjectDoesNotExist):
        public_draftable_object.public_version


@pytest.mark.django_db
def test_cannot_create_draft_from_public_already_having_draft():
    draft_draftable_object = create_draftable_object(
        version_status=VersionStatus.DRAFT,
        many_to_many_objects=["m2m1", "m2m2", "m2m3"],
        many_to_many_2_objects_through_objects=["m2mt1", "m2mt2", "m2mt3"],
        one_to_one_related_object=True,
    )

    public_draftable_object = draft_draftable_object.publish_draft()  # noqa

    with pytest.raises(DraftVersionExistsError):
        public_draftable_object.create_draft()


@pytest.mark.django_db
def test_cannot_create_draft_from_malformed_public_having_both_public_and_draft():
    draft_draftable_object = create_draftable_object(
        version_status=VersionStatus.DRAFT,
        many_to_many_objects=["m2m1", "m2m2", "m2m3"],
        many_to_many_2_objects_through_objects=["m2mt1", "m2mt2", "m2mt3"],
        one_to_one_related_object=True,
    )
    public_draftable_object = draft_draftable_object.publish_draft()  # noqa

    DraftableObjectFactory(draft_version=public_draftable_object)

    with pytest.raises(InvalidVersionError):
        public_draftable_object.create_draft()


@pytest.mark.django_db
def test_can_create_draft_from_public_with_no_draft():
    public_draftable_object = create_draftable_object(
        version_status=VersionStatus.PUBLIC,
        many_to_many_objects=["m2m1", "m2m2", "m2m3"],
        many_to_many_2_objects_through_objects=["m2mt1", "m2mt2", "m2mt3"],
        one_to_one_related_object=True,
    )

    draft_draftable_object = public_draftable_object.create_draft()  # noqa

    assert draft_draftable_object.is_draft
    assert not draft_draftable_object.is_public
    assert public_draftable_object.draft_version == draft_draftable_object

    assert_draft_and_public_draftable_objects_are_equal(
        draft_draftable_object, public_draftable_object
    )
