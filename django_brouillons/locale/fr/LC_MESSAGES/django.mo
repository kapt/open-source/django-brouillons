��          �      L      �     �     �     �     �     �          )     >     U     k     }     �     �     �  &   �     �     �       g       m     �     �     �     �     �     �          3     Q     j     n          �  -   �  #   �     �                                                                                     
             	       Changes published Create a draft Creation date Draft version First publication date Go to draft version Go to public version Last modification date Last publication date New draft created No Public version Publication successful Publish draft The last changes are already published There are unpublished changes Version Yes Project-Id-Version: 0.0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-05 16:34+0100
Last-Translator: Benjamin PIERRE <contact@benjamin-pierre.dev>
Language-Team: Kapt <dev@kapt.mobi>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.4.2
 Changements publiés Créer une version brouillon Date de création Version brouillon Date de première publication Voir la version brouillon Voir la version publique Date de dernière modification Date de dernière publication Nouveau brouillon créé Non Version publique Publication réussie Publier le brouillon Les derniers changements sont déjà publiés Il y a des changements non publiés Version Oui 