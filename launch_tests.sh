#!/bin/bash
# This script is used to launch the tests
poetry run pytest --ds=tests.settings --spec -s "$@"
