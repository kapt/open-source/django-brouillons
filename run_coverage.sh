#!/bin/bash
# This script is used to launch the tests
poetry run coverage run
poetry run coverage report
poetry run coverage xml
poetry run coverage html
